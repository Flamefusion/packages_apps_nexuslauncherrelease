include $(call first-makefiles-under,$(LOCAL_PATH))

PRODUCT_PACKAGES += \
    MatchmakerPrebuiltPixel4 \
    QuickAccessWallet \
    NexusLauncherRelease \
    PixelConfigOverlayCommon \
	Pixel-LiveWall

PRODUCT_COPY_FILES += \
	  packages/apps/NexusLauncherRelease/etc/permissions/com.android.launcher3.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.android.launcher3.xml \
	  packages/apps/NexusLauncherRelease/etc/permissions/com.android.systemui.plugin.globalactions.wallet.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.android.systemui.plugin.globalactions.wallet.xml \
	  packages/apps/NexusLauncherRelease/etc/permissions/com.google.android.apps.nexuslauncher.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.apps.nexuslauncher.xml \
	  packages/apps/NexusLauncherRelease/etc/permissions/com.google.android.as.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.as.xml \
	  packages/apps/NexusLauncherRelease/etc/permissions/privapp-permissions-com.google.android.apps.nexuslauncher.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-com.google.android.apps.nexuslauncher.xml \
	  packages/apps/NexusLauncherRelease/etc/sysconfig/hiddenapi-whitelist-com.google.android.apps.nexuslauncher.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-whitelist-com.google.android.apps.nexuslauncher.xml
# libs for MatchmakerPrebuiltPixel4
PRODUCT_COPY_FILES += \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libaiai-annotators.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libaiai-annotators.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libbarhopper_v2.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libbarhopper_v2.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libBrella.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libBrella.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libclient_android_jni.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libclient_android_jni.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libcpuutils.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libcpuutils.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libdps_soda_jni.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libdps_soda_jni.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libim2intent_jni.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libim2intent_jni.so \
          packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libparticle-extractor_jni.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libparticle-extractor_jni.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libsense.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libsense.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libparticle-extractor_jni.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libparticle-extractor_jni.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libsimple_itracker_jni.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libsimple_itracker_jni.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libtensorflowlite_jni.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libtensorflowlite_jni.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libtextclassifier3_jni_aiai.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libtextclassifier3_jni_aiai.so \
	  packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libtrainingfeatureprocessor_jni_gmscore.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libtrainingfeatureprocessor_jni_gmscore.so \
          packages/apps/NexusLauncherRelease/app/MatchmakerPrebuiltPixel4/lib/arm64/libaiai_vkp.so:$(TARGET_COPY_OUT_SYSTEM)/product/priv-app/MatchmakerPrebuiltPixel4/lib/arm64/libaiai_vkp.so \
