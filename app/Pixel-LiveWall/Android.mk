LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := Pixel-LiveWall
LOCAL_MODULE_STEM := Pixel-LiveWall.apk
LOCAL_SRC_FILES := Pixel-LiveWall.apk
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(TARGET_OUT)/product/priv-app/Pixel-LiveWall

include $(BUILD_PREBUILT)
