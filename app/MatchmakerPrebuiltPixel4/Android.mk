LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := MatchmakerPrebuiltPixel4
LOCAL_MODULE_STEM := MatchmakerPrebuiltPixel4.apk
LOCAL_SRC_FILES := MatchmakerPrebuiltPixel4.apk
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(TARGET_OUT)/product/priv-app/MatchmakerPrebuiltPixel4

include $(BUILD_PREBUILT)