# NexusLauncherRelease (Android R)
A make-file build module to prebuilt google pixel launcher in ROMS that ship other launcher.

This replaces other launchers in source, if you dont want to replace them  then https://gitlab.com/Flamefusion/packages_apps_nexuslauncherrelease/-/blob/master/priv-app/NexusLauncherRelease/Android.mk#L7 remove this line.

It includes matchmakerprebuilt from pixel 4, QuickAccesssWallet from pixel, pixel overlay common.

Included Pixel-LiveWall app from XDA, It have all the pixel live walls in it. Thanx to the developer who ported this amazing app.
