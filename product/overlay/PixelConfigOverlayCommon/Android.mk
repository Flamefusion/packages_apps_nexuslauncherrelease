LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := PixelConfigOverlayCommon
LOCAL_MODULE_STEM := PixelConfigOverlayCommon.apk
LOCAL_SRC_FILES := PixelConfigOverlayCommon.apk
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(TARGET_OUT)/product/overlay/PixelConfigOverlayCommon

include $(BUILD_PREBUILT)
